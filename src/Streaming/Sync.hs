{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Streaming.Sync
  ( mergeSemigroup
  , logChanges
  , Update (..)
  , _Update
  , _Delete
  , _New
  , Equality
  , computeUpdates
  , computeUpdatesEq
  , computeAssocsUpdates
  ) where

import qualified Control.Foldl as L
import Control.Lens (makePrisms)
import qualified Data.Map.Monoidal.Strict as Mn
import Protolude hiding (Last)
import Streaming (Of (..), Stream)
import Streaming.Concat (sconcatS)
import qualified Streaming.Prelude as S
import Data.These ( These(..) )
import Data.Semigroup ( Last(..) )

mergeSemigroup
  :: (Ord t, Monad m, Semigroup b)
  => Stream (Of (t, b)) m r
  -> Stream (Of (t, b)) m s
  -> Stream (Of (t, b)) m (r, s)
mergeSemigroup x = sconcatS . S.mergeOn fst x

type LRB a b = These (Last a) (Last b)

mkL :: a -> LRB a b
mkL x = This (Last x)

mkR :: b -> LRB a b
mkR y = That (Last y)

data Update q v
  = Update (q, v) v
  | Delete q
  | New v
  deriving (Eq, Show)

makePrisms ''Update

type Equality a = a -> a -> Bool

logChanges :: (Int -> Int -> Int -> a) -> L.Fold (Update b v) a
logChanges f =
  f
    <$> do L.handles _Update L.length
    <*> do L.handles _New L.length
    <*> do L.handles _Delete L.length

lrb2Update :: (Eq v) => LRB (q, v) v -> Maybe (Update q v)
lrb2Update = lrb2UpdateEq (==)

lrb2UpdateEq :: Equality v -> LRB (q, v) v -> Maybe (Update q v)
lrb2UpdateEq eq (These (Last (q, x)) (Last y))
  | x `eq` y = Nothing
  | otherwise = Just $ Update (q, x) y
lrb2UpdateEq _ (This (Last (q, _))) = Just (Delete q)
lrb2UpdateEq _ (That (Last v)) = Just (New v)

-- | See 'computesUpdatesEq'
computeUpdates
  :: (Monad m, Ord t, Eq a)
  => Stream (Of (t, (q, a))) m r
  -> Stream (Of (t, a)) m s
  -> Stream (Of (t, Update q a)) m (r, s)
computeUpdates = computeUpdatesEq (==)

-- | Computes the differences of two streams,
-- i.e. updates that would need to be performed on the first
-- stream to obtain the second stream.
-- 
-- The stream elements are tuples where the first component (key) @t@ must be strictly monotonically increasing (= ordered and unique).
--
-- * A key is missing in the first stream but present in the second yields a 'new' update element.
-- * A key present in the first stream but missing in the second yields a 'delete' update element.
-- * A key present in both streams yields an 'update' element only if the second component @a@ is 
-- considered different under the given equality function.
--
-- The first stream also carries an additional payload @q@, which will not be discarded. This payload
-- will be part of resulting 'delete' and 'update' elements.
computeUpdatesEq
  :: (Monad m, Ord t)
  => Equality a
  -> Stream (Of (t, (q, a))) m r
  -> Stream (Of (t, a)) m s
  -> Stream (Of (t, Update q a)) m (r, s)
computeUpdatesEq equality old new =
  S.for
    do mergeSemigroup (prepare mkL old) (prepare mkR new)
    do
      \(t, x) -> case lrb2UpdateEq equality x of
        Nothing -> pure ()
        Just u -> S.yield (t, u)
  where
    prepare f = sconcatS . S.map (fmap f)

computeFunctorUpdates
  :: (Monad m, Functor f, Ord t, Semigroup (f (LRB (q, a) a)), Eq a)
  => Stream (Of (t, f (q, a))) m r
  -> Stream (Of (t, f a)) m s
  -> Stream (Of (t, f (Maybe (Update q a)))) m (r, s)
computeFunctorUpdates old new =
  S.map
    do fmap $ fmap lrb2Update
    do mergeSemigroup (prepare mkL old) (prepare mkR new)
  where
    prepare f = sconcatS . S.map (fmap $ fmap f)

computeAssocsUpdates
  :: (Monad m, Ord t, Ord k, Eq v)
  => Stream (Of (t, (k, (q, v)))) m r
  -> Stream (Of (t, (k, v))) m s
  -> Stream (Of (t, (k, Update q v))) m (r, s)
computeAssocsUpdates s1 s2 =
  S.for
    do computeFunctorUpdates (S.map prepare s1) (S.map prepare s2)
    do \(t, q) -> S.catMaybes $ S.each $ (\(k, v) -> (t,) . (k,) <$> v) <$> Mn.assocs q
  where
    prepare (t, (k, v)) = (t, Mn.singleton k v)
