{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}

module Streaming.Intercalate where

import Protolude
import Streaming
import qualified Streaming.Prelude as S

intercalateS :: Monad m => (b -> b -> [b]) -> Stream (Of b) m r 
  -> Stream (Of b) m r
intercalateS f s = S.for
  do tailS s
  do
    \(x, my) -> do
      traverse_ S.yield (maybeToList my >>= flip f x)
      S.yield x

tailS :: Monad m => Stream (Of a) m r -> Stream (Of (a, Maybe a)) m r
tailS = S.scanned
  do \(_, p) x -> (p, Just x)
  do (panic "tailS", Nothing)
  do fst
