{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Streaming.Sync.Consume
  ( consumeUpdates
  ) where

import Protolude
import Streaming (Of (..), Stream, effect, hoist)
import qualified Streaming as S
import qualified Streaming.Prelude as S
import Streaming.Sync (Update (Delete, New, Update))


-- data OutputF q v f = OutputF 
--   { output_new :: f v
--   , output_delete :: f q 
--   , output_update :: f ((q, v), v) 
--   }


type Output q v m = Stream (Of v) (Stream (Of q) (Stream (Of ((q, v), v)) m))

plex
  :: forall q v m b.
  (Monad m)
  => Stream (Of (Update q v)) m b
  -> Output q v m b
plex = S.effects . go . hoist (lift . lift . lift)
  where
    go s' = effect do
      ex <- S.inspect s'
      pure $ case ex of
        Left b -> pure b
        Right (x :> s'') -> case x of
          Update (q, v1) v2 -> do
            lift . lift $ lift $ S.yield ((q, v1), v2)
            go s''
          Delete q -> do
            lift . lift $ S.yield q
            go s''
          New v -> do
            lift $ S.yield v
            go s''

sendGroups :: Monad m => Int -> ([a] -> m ()) -> Stream (Of a) m r -> m r
sendGroups s f = S.mapM_ f . S.mapped S.toList . S.chunksOf s

consumeUpdates
  :: forall q v m b .
  (Monad m)
  => Int -- ^ batch size  
  -> ([v] -> m ()) -- ^  how to burn a batch of new 
  -> ([q] -> m ()) -- ^ how to burn a batch of delete
  -> ([((q, v), v)] -> m ()) -- ^ how to burn a batch of updates
  -> Stream (Of (Update q v)) m b -- ^ input
  -> m b
consumeUpdates s new delete update =
  sendGroups s update
    . sendGroups s (lift . delete)
    . sendGroups s (lift . lift . new)
    . plex
