{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Streaming.Sync.Batch
  ( BatchUpdate (..)
  , groupBatch
  ) where

import Streaming.Sync (Update (..))
import Data.Dependent.Map (DMap)
import qualified Data.Dependent.Map as DM
import Data.Dependent.Sum (DSum (..))
import Data.GADT.Compare
  ( GCompare (..)
  , GEq (..)
  , GOrdering (GEQ, GGT, GLT)
  )
import Data.GADT.Show (GShow, gshowsPrec)
import Data.Some ()
import Protolude
  ( Applicative (pure)
  , Either (Left, Right)
  , Functor
  , Identity (Identity)
  , Int
  , Maybe (..)
  , Monad ((>>))
  , Monoid (mempty)
  , Num ((+))
  , Ord (compare, (>=))
  , Ordering (EQ, GT, LT)
  , Semigroup ((<>))
  , Show
  , panic
  , ($)
  , type (:~:) (Refl)
  )
import Protolude.Base (showsPrec)
import Streaming (Of (..), Stream, effect, inspect)
import qualified Streaming.Prelude as S

data BatchUpdate q v x where
  BatchUpdate :: BatchUpdate q v ((q, v), v)
  BatchDelete :: BatchUpdate q v q
  BatchInsert :: BatchUpdate q v v

deriving instance Show (BatchUpdate q v x)

instance GShow (BatchUpdate q v) where
  gshowsPrec = showsPrec

instance GEq (BatchUpdate q v) where
  geq BatchUpdate BatchUpdate = Just Refl
  geq BatchDelete BatchDelete = Just Refl
  geq BatchInsert BatchInsert = Just Refl
  geq _ _ = Nothing

instance GCompare (BatchUpdate q v) where
  gcompare x y = case geq x y of
    Just Refl -> GEQ
    Nothing -> case compare (ordBU x) (ordBU y) of
      LT -> GLT
      GT -> GGT
      EQ -> panic "no way GCompare works"
    where
      ordBU :: BatchUpdate q v x -> Int
      ordBU BatchUpdate = 0
      ordBU BatchDelete = 1
      ordBU BatchInsert = 2

data Collect a = Collect {_collectSize :: Int, getBatch :: [a]} deriving (Functor)

sumCollect :: Collect a -> Collect a -> Collect a
sumCollect (Collect n xs) (Collect m ys) = Collect (n + m) $ xs <> ys

type BatchMap q v = DMap (BatchUpdate q v) Collect

newUpdate :: Update q v -> DSum (BatchUpdate q v) Identity -- , Some (BatchUpdate q v))
newUpdate (Delete q) = BatchDelete :=> Identity q
newUpdate (Update qv v) = BatchUpdate :=> Identity (qv, v)
newUpdate (New v) = BatchInsert :=> Identity v

groupBatch
  :: forall m q v b. (Monad m)
  => Int -- ^ batch size
  -> Stream (Of (Update q v)) m b
  -> Stream (Of (DSum (BatchUpdate q v) [])) m b
groupBatch dumpSize = go mempty
  where
    go
      :: BatchMap q v
      -> Stream (Of (Update q v)) m b
      -> Stream (Of (DSum (BatchUpdate q v) [])) m b
    go m s = effect do
      ex <- inspect s
      pure $ case ex of
        Left b -> S.each (DM.assocs $ DM.map getBatch m) >> pure b
        Right (x :> s') -> case newUpdate x of
          k :=> Identity x' ->
            let m'' = DM.insertWith sumCollect k (Collect 1 [x']) m
                Collect l ys = m'' DM.! k
             in if l >= dumpSize
                  then S.yield (k :=> ys) >> go (DM.delete k m'') s'
                  else go m'' s'