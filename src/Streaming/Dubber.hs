{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Streaming.Dubber where

import qualified Data.Map.Strict as M
import Protolude hiding (StateT, evalStateT) 
import Streaming ( Stream, Of(..), distribute, effect )
import Streaming.Internal ( Stream(Step, Return, Effect) )
import qualified Streaming.Prelude as S
import Control.Monad.State.Strict (StateT, evalStateT)

type Queue k v = Map k (NonEmpty v)

add :: Ord k => [(k, x)] -> Queue k x -> Queue k x
add xs m = foldr (\(k, x) -> M.insertWith (<>) k (x :| [])) m xs

produceRight
  :: (Monad m, Foldable t)
  => Map k (t b)
  -> Stream (Of (k, Either a b)) m ()
produceRight rest =
  S.each do
    (k, eventsList) <- M.assocs rest
    ev <- toList eventsList
    pure (k, Right ev)

-- stream intercalates auto generated events in  the  future
-- 'k' is required to be non decreasing in the generator stream
-- 'k' is required to be in bigger then the argument in the generator function
dubber
  :: forall m k x y r.
  (Monad m, Ord k)
  => (k -> x -> m [(k, y)]) -- generator function
  -> Stream (Of (k, x)) m r -- generator stream
  -> Stream (Of (k, Either x y)) m r -- mixed generator stream and generated stream
dubber shoot s = flip evalStateT (mempty :: Queue k y) $ distribute $ mergeFeed s
  where
    mergeFeed :: Stream (Of (k, x)) m r -> Stream (Of (k, Either x y)) (StateT (Queue k y) m) r
    mergeFeed (Return r) = effect $ do
      rest <- get
      pure $ r <$ produceRight rest
    mergeFeed (Effect m) = effect $ mergeFeed <$> lift m
    mergeFeed (Step ((k, x) :> rx)) = effect $ do
      lift (shoot k x) >>= modify . add 
      (now, later, rest') <- gets $ M.splitLookup k 
      put rest'
      pure do
        produceRight $ M.unionWith (<>) now (maybe mempty (M.singleton k) later)
        S.yield (k, Left x)
        mergeFeed rx
