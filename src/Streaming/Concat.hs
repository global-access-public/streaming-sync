{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Streaming.Concat where

import Protolude (Bifunctor (first, second), on)
import Streaming (Of ((:>)), Stream)
import Streaming.NonEmpty (NEStream (NEStream))
import qualified Streaming.NonEmpty as NES
import qualified Streaming.Prelude as S
import qualified Control.Foldl.NonEmpty as L1

sconcatS
  :: (Eq a, Semigroup c, Monad m)
  => Stream (Of (a, c)) m r
  -> Stream (Of (a, c)) m r
sconcatS =
  S.mapped do
    \(NEStream ((t, x) :> s)) ->
      first (t,) <$> sconcat (NEStream $ x :> S.map snd s)
    . NES.groupBy ((==) `on` fst)
  where
    sconcat = L1.purely NES.fold1 L1.sconcat

shwartzS
  :: Monad m
  => (a -> b)
  -> (b -> a)
  -> (Stream (Of (t, b)) m r -> Stream (Of (t, b)) m r)
  -> Stream (Of (t, a)) m r
  -> Stream (Of (t, a)) m r
shwartzS f h g = S.map (second h) . g . S.map (second f)
