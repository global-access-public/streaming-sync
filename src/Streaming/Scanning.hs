{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}

module Streaming.Scanning where

import Protolude
import Streaming
import qualified Streaming.Prelude as S

scanMonoid
  :: (Monad m, Monoid w)
  => Stream (Of (k, w)) m r
  -> Stream (Of (k, w)) m r
scanMonoid =
  S.drop 1 . S.scan
    do \(_, m) (d, m') -> (d, m <> m')
    do (panic "use non-empty", mempty)
    do identity 

-- scanSemigroup
--   :: (Monad m, Semigroup w)
--   => NES.Stream (Of (k, w)) m r
--   -> NES.Stream (Of (k, w)) m r
-- scanSemigroup =
--   S.drop 1 . S.scan
--     do \(_, m) (d, m') -> (d, m <> m')
--     do (panic "use non-empty", mempty)
--     do identity