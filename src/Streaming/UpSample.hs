{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE TupleSections #-}

module Streaming.UpSample where

import Streaming.Intercalate (intercalateS)
import Streaming (Of (..), Stream)
import qualified Streaming.Prelude as S

-- | add intermediate values to a stream repeating as necessary
upSample
  :: (Monad m, Ord t)
  => (t -> t) -- ^ time step
  -> Stream (Of (t, b)) m r
  -> Stream (Of (t, b)) m r
upSample step = intercalateS \(d, a) (d', _) -> do
  fmap (,a) $ takeWhile (< d') $ tail $ iterate step d

fillUp
  :: (Monad m, Ord t)
  => t -- ^ until
  -> (t -> t) -- ^ time step
  -> Stream (Of (t, a)) m r
  -> Stream (Of (t, a)) m r
fillUp end step s = do
  (ml :> r) <- S.store S.last s
  case ml of
    Nothing -> pure r
    Just l@(lend, v) -> case compare lend end of
      LT ->
        S.drop 1 $
          upSample step $
            S.yield l >> S.yield (end, v) >> pure r
      _ -> pure r

upSampleAndFillUp
  :: (Monad m, Ord t)
  => t
  -> (t -> t)
  -> Stream (Of (t, a)) m r
  -> Stream (Of (t, a)) m r
upSampleAndFillUp end step = fillUp end step . upSample step
