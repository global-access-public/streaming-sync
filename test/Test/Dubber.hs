{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module Test.Dubber where

import GHC.Base (String)
import Streaming.Dubber ( dubber )
import Protolude
    ( zip,
      ($),
      Num((+)),
      Applicative(pure),
      Int,
      Either(..),
      (<$>),
      show,
      Identity(runIdentity),
      Text )
import qualified Streaming.Prelude as S
import Test.Hspec ( it, shouldBe, Spec )

-- spec_dubber :: IO ()
spec_dubber :: Spec
spec_dubber = do
  it "does nothing on empty dubbing" do
    shouldBe
      do
        runIdentity $
          S.toList_ $ dubber
            (\_ _ -> pure [])
            do S.each $ zip [1 .. 10] [1 .. 10]
      do zip [1 .. 10] $ Left <$> [1 .. 10] :: [(Int, Either Int String)]
  it "insert dubs after 1 " do
    shouldBe
      do
        runIdentity $
          S.toList_ $
            dubber (\k x -> pure [(k + 1, show @_ @Text x)]) $
              S.each $
                zip [1 .. 3] [1 .. 3]
      do
        [ (1, Left 1)
          , (2, Right "1")
          , (2, Left 2)
          , (3, Right "2")
          , (3, Left 3)
          , (4, Right "3")
          ]
  it "insert dubs after 0 " do
    shouldBe
      do
        runIdentity $
          S.toList_ $
            dubber (\k x -> pure [(k, show @_ @Text x)]) $
              S.each $
                zip [1 .. 3] [1 .. 3]
      do
        [ 
          (1, Right "1")
          , (1, Left 1)
          , (2, Right "2")
          , (2, Left 2)
          , (3, Right "3")
          , (3, Left 3)
          ]
