{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module Test.Streaming where

import Streaming.Scanning ( scanMonoid )
import Streaming.UpSample ( fillUp, upSample )
import Protolude
import qualified Streaming.Prelude as S
import Test.Tasty ( TestTree )
import Test.Hspec (shouldBe)
import Streaming.Intercalate ( tailS )
import Test.FreeCase ( testCaseF )
import Lib.FreeList

type L = [(Int, Text)]



test_scanMonoid :: [TestTree]
test_scanMonoid = freeListOf do
  testCaseF "no one is no one " $ shouldBe
      do S.toList_ $ scanMonoid $ S.each ([] :: L)
      do Identity []
  testCaseF "one mempty" $ shouldBe
      do S.toList_ $ scanMonoid $ S.each [(1, mempty :: Text)]
      do Identity [(1, "")]
  testCaseF "2 are 2" $ shouldBe
      do S.toList_ $ scanMonoid $ S.each [(1, mempty :: Text), (2, mempty)]
      do Identity [(1, ""), (2, "")]
  testCaseF "monoid are summed" $ shouldBe
      do S.toList_ $ scanMonoid $ S.each [(1, "hello " :: Text), (2, "world")]
      do Identity [(1, "hello "), (2, "hello world")]

test_upSample :: [TestTree]
test_upSample = freeListOf do
  testCaseF "no one is no one" $ shouldBe
      do S.toList_ $ upSample succ $ S.each ([] :: L)
      do Identity []
  testCaseF "one elem cannot be upsampled" $ shouldBe
      do S.toList_ $ upSample succ $ S.each [(1, mempty :: Text)]
      do Identity [(1, "")]
  testCaseF "2 adjacent are not upsampled" $ shouldBe
      do S.toList_ $ upSample succ $ S.each [(1, mempty :: Text), (2, "")]
      do Identity [(1, ""), (2, "")]
  testCaseF "2 not adjacent are not upsampled repating first" $ shouldBe
      do S.toList_ $ upSample succ $ S.each [(1, "hello" :: Text), (3, "world")]
      do Identity [(1, "hello"), (2, "hello"), (3, "world")]

test_fillUp :: [TestTree]
test_fillUp  = freeListOf do
  testCaseF "no one is no one" $ shouldBe
      do S.toList_ $ fillUp 10 succ $ S.each ([] :: L)
      do Identity []
  testCaseF "one elem up to itself doesn't fill" $ shouldBe
      do S.toList_ $ fillUp 1 succ $ S.each [(1, mempty :: Text)]
      do Identity [(1, "")]
  testCaseF "1 element fill up itself to the next one" $ shouldBe
      do S.toList_ $ fillUp 2 succ $ S.each [(1, mempty :: Text)]
      do Identity [(1, ""), (2, "")]


test_tailS :: [TestTree]
test_tailS = freeListOf do
  testCaseF "no one is no one" $ shouldBe
      do S.toList_ $ tailS $ S.each ([] :: L)
      do Identity []
