{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables, TypeApplications #-}

module Test.Sync where
-- this is a non-streaming reference imlementation based on Data.Map difference and intersection

import Data.Map (Map)
import qualified Data.Map as M
import Streaming.Sync
import Test.Hspec
import Test.QuickCheck
import qualified Streaming.Prelude as S
import Streaming

referenceDiffs :: (Ord t, Ord k, Eq v) => [(t, (k, (q, v)))] -> [(t, (k, v))] -> [(t, (k, Update q v))]
referenceDiffs s1 s2 =
  let m1 = M.fromList $ fmap (\(t, (k, (_, v))) -> ((t, k), v)) s1
      qs = M.fromList $ fmap (\(t, (k, (q, _))) -> ((t, k), q)) s1
      m2 = M.fromList $ fmap (\(t, (k, v)) -> ((t, k), v)) s2
      delete = Delete <$> getQ qs (M.difference m1 m2)
      new = New <$> (M.difference m2 m1)
      update = M.filter (\(Update (_,v1) v2) -> v1 /= v2) $ M.intersectionWithKey (\tk v1 v2 -> Update (qs M.! tk, v1) v2) m1 m2
   in fmap (\((t, k), v) -> (t, (k, v))) $ M.assocs $ delete <> update <> new

getQ :: (Ord t, Ord k) => Map (t, k) q -> Map (t, k) v -> Map (t, k) q
getQ = M.mergeWithKey (\_ x _ -> Just x) (const mempty) (const mempty)

randomList :: Int -> Gen [(Int, (Int, (Int, Int)))]
randomList r = do
  l :: Int <- choose (1, r)
  let next l' n m x
        | l' == l = pure []
        | otherwise = do
          cn <- elements [False, False, False, True] -- time speed
          cd <- choose (1, 2)
          let (n', m') = if cn then (n + cd, cd - 1) else (n, m + 1) -- don't repeat keys in the same time
          cm <- elements [False, False, True] -- key speed
          cd <- choose (1, 2)
          let (m'', x') = if cm then (m' - cd, cd + 1) else (m', x)
          cx <- elements [False, False, False, True] -- value change
          cd <- choose (1, 2)
          let x'' = if cx then x' + cd else x'
          ((n', (m'', (l', x''))) :) <$> next (l' + 1) n' m'' x''
  next 0 0 0 0

stripQ = fmap \(t,(k,(_, v))) -> (t,(k,v))

-- renounce the streaming in the tests
runStreamDiffs :: (Ord t, Ord k, Eq v, Show v, Show t, Show k, Show q) => [(t, (k, (q,v)))] -> [(t, (k, v))] -> [(t, (k, Update q v))]
runStreamDiffs s1 s2 =
  let ls :> _ = runIdentity $ S.toList $ computeAssocsUpdates (S.each s1) (S.each s2)
   in ls

runStreamDiffsInt 
  :: [(Int, (Int, (Int, Int)))]
  -> [(Int, (Int, Int))] -> [(Int, (Int, Update Int Int))]
runStreamDiffsInt = runStreamDiffs @Int @Int @Int @Int

referenceDiffsInt
  :: [(Int, (Int, (Int, Int)))]
  -> [(Int, (Int, Int))] -> [(Int, (Int, Update Int Int))]
referenceDiffsInt = referenceDiffs @Int @Int @Int @Int

spec_sync :: SpecWith ()
spec_sync = do
  describe "streamDiffs" do
    it "produces no diffs for same stream" $
      shouldBe
        do referenceDiffsInt [(1, (1, (0,1)))] [(1, (1, 1))]
        do []
    it "produces deletes for left stream " $
      shouldBe
        do referenceDiffsInt [(1, (1, (0,1)))] []
        do [(1, (1, Delete 0))]
    it "produces news for right stream " $
      shouldBe
        do referenceDiffsInt [] [(1, (1, 1))]
        do [(1, (1, New @Int 1))]
    it "produces delete news for uncrossing streams " $
      shouldBe
        do referenceDiffsInt [(1, (1, (0,1)))] [(2, (1, 1))]
        do [(1, (1, Delete 0)), (2, (1, New 1))]
    it "produces Updates for different crossing streams" $
      shouldBe
        do referenceDiffsInt [(1, (1, (0,1)))] [(1, (1, 2))]
        do [(1, (1, Update (0,1) 2))]
    it "produces no diffs for same stream" $
      shouldBe
        do runStreamDiffsInt [(1, (1, (0,1)))] [(1, (1, 1))]
        do []
    it "produces deletes for left stream " $
      shouldBe
        do runStreamDiffsInt [(1, (1, (0,1)))] []
        do [(1, (1, Delete 0))]
    it "produces news for right stream " $
      shouldBe
        do runStreamDiffsInt [] [(1, (1, 1))]
        do [(1, (1, New @Int 1))]
    it "produces delete news for uncrossing streams " $
      shouldBe
        do runStreamDiffsInt [(1, (1, (0,1)))] [(2, (1, 1))]
        do [(1, (1, Delete 0)), (2, (1, New 1))]
    it "produces Updates for different crossing streams" $
      shouldBe
        do runStreamDiffsInt [(1, (1, (0,1)))] [(1, (1, 2))]
        do [(1, (1, Update (0,1) 2))]
    it "match the pure implementation" $
      property $
        forAll ((,) <$> randomList 1000 <*> randomList 1000) $ \(l1, l2) -> shouldBe
          do referenceDiffs l1 $ stripQ l2
          do runStreamDiffs l1 $ stripQ l2
